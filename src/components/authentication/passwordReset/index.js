import React, {Component} from 'react';
import { View, Keyboard } from 'react-native';
import styles from "../styles";
import {  Container, Content, Form, Item, Input, Label, Icon, Button, Text } from 'native-base';
import { validateEmail } from '../../../utils/validators';

import { connect } from 'react-redux';
import { forgotPassword } from '../../../store/actions/user_actions';
import { bindActionCreators } from 'redux';

class PasswordReset extends Component {
    static navigationOptions = {
        title: "Forgot Password",
      };

      constructor() {
        super();
        this.state = {
            email: '',
            resetParamsValid:false,
            emailValid:false,
            emailError:false,
            emailIcon:null,
            enableReset:false,
            showToast: false,
        };
      }

      updateEmailUI = (valid, cb) => {
        this.setState({
          emailIcon: valid ? 'checkmark-circle':'close-circle',
          emailError:!valid,
          emailValid:valid
        },
        cb)
      }

      validateForm = () => {
        let { emailValid, email} = this.state;
        console.log(emailValid);
        console.log(email);
        this.setState({
            email:email,
            enableReset:emailValid
          });
      }

      onEmailChange = (email) => {
        this.setState({ email: email });
        this.updateEmailUI(validateEmail(email), function(){
          this.validateForm();
        });
      };

      resetPassword = () => {
        Keyboard.dismiss();
        this.props.forgotPassword(this.state.email).then((response) => {
            console.log('forgot password response');
            setTimeout(
                () => {
                    this.props.navigation.navigate('SignIn');
                },
                5000
            );
        });
      }

    render () {
        let { 
            emailError, 
            emailValid, 
            email, 
            emailIcon,
            enableReset
        } = this.state;

        return (
            <Container style={styles.container}>
                <Content padder>
                    <View class={{ width:'100%'}}>
                        <Form>
                        <Item floatingLabel success={emailValid} error={emailError}>
                            <Label>Email</Label>
                            <Input 
                                placeholder="" 
                                value={email}
                                keyboardType="email-address"
                                blurOnSubmit={true}
                                onSubmitEditing={() => { this.validateForm();  }}
                                onChangeText={(text) => this.onEmailChange(text)}/>

                            { (emailValid || emailError) &&
                                <Icon name={emailIcon}/>
                            }
                            </Item>
                        </Form>
                        <View style={{left:'5%', width:'90%', marginTop:20}}>
                            <Button block disabled={!enableReset} onPress = {() => this.resetPassword()}>
                                <Text>Reset</Text>
                            </Button>
                        </View>
                    </View>
                </Content>
            </Container>
        ); 
    }
}

function mapStateToProps(state){
    return {
      user : state.User
    }
  }
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators({forgotPassword}, dispatch);
  }

  export default connect(mapStateToProps, mapDispatchToProps)(PasswordReset);