import React, { Component } from "react";
import { View, StatusBar, AsyncStorage, Animated, Dimensions, Keyboard, ActivityIndicator } from "react-native";
import { Container, Button, Text, Content } from "native-base";

import { connect } from 'react-redux';
import { login } from '../../../store/actions/user_actions';
import { bindActionCreators } from 'redux';

import styles from "../styles";

const launchscreenLogo = require("../../../assets/logo_ico.png");
const deviceHeight = Dimensions.get("window").height;

const logoStart = deviceHeight / 4;
const logoEnd = deviceHeight / 16;
const marginTop = deviceHeight / 8;
const logoStartSize = 200;
const logoEndSize = 100;

import LoginForm from './form';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
          logoPosition: new Animated.Value(logoStart), 
          logoSize: new Animated.Value(logoStartSize), 
          formOpacity: new Animated.Value(0.2), 
          email:null,
          password:null,
          enableLogin:false,
          showToast: false,
          loading:true
        }
      }

    static navigationOptions = {
        header: null,
      };
      
    componentDidMount() {
      //console.log(this.props.user);
      if(this.props.user.loggedIn){
        this._skip();
      } else {
        this.showLogin();
      }
    }

    showLogin = () => {
      console.log('showing the login screen')
      this.setState({loading:false});
      Animated.timing( 
        this.state.logoPosition,            
        {
          toValue: logoEnd,         
          duration: 1000,       
        }
      ).start();  

      Animated.timing( 
        this.state.logoSize,            
        {
          toValue: logoEndSize,         
          duration: 1000,       
        }
      ).start();  
    }

    updateRegistrationStatus(props) {
      if(props.user.loggedIn){
        props.navigation.navigate('App');
      }
    }

    loginUser = () => {
      Keyboard.dismiss();
      this.props.login(this.state)
      .then(() => {
        this.updateRegistrationStatus(this.props);
      })
      .catch((err) => {
        console.log("ERROR in logging in");
      });
    }

    _register = () => {
        this.props.navigation.navigate('Register');
    }

    forgotPassword = () => {
      this.props.navigation.navigate('ForgotPassword');
    }

    _skip() {
      this.props.navigation.navigate('App');
    }

    

      
    

    enableLoginUser = (email, password) => {
      let enableLogin = (email !== null && password !== null);

      this.setState({
        email:email,
        password:password,
        enableLogin:enableLogin
      });
    }

    render() {
        let { logoPosition, logoSize, enableLogin, loading } = this.state;

        if(loading) {
          return (
            <Container style={styles.container}>
            <Content padder>
              <View style={styles.loading}>
                <ActivityIndicator/>
              </View>
            </Content>
          </Container>
          );
        } else {
          return (
            <Container style={styles.container}>
              <Content padder>
              <Animated.View style={{
                marginTop:logoPosition, 
                alignItems: 'center',
                width:'100%'}}>
                  <Animated.Image source={launchscreenLogo} style={{
                    height:logoSize, 
                    width:logoSize, 
                    borderRadius:10}} />
              </Animated.View>
        
              <View class={{marginTop:marginTop, width:'100%'}}>
                <LoginForm enableLoginUser={this.enableLoginUser}/>
  
                <View style={{left:'5%', width:'90%', marginTop:20}}>
                  <Button block disabled={!enableLogin} onPress = {() => this.loginUser()}>
                    <Text>Log in</Text>
                  </Button>
                </View>
  
                <View style={styles.container2}>
                <Button transparent style={styles.textButton} onPress = {() => this._register()}>
                  <Text style={{fontSize:14}}>Create Account</Text>
                </Button>
                  
                  <Button transparent style={styles.textButton} onPress = {() => this.forgotPassword()}>
                    <Text style={{fontSize:14}}>Forgot Password</Text>
                  </Button>
                </View>
              </View>
  
              <View style={{ marginTop:30, width:'100%'}}>
              <Button bordered info onPress = {() => this._skip()}>
                    <Text style={{textAlign:'right'}}>Skip</Text>
                  </Button>
              </View>
         
  
              </Content>
              
            </Container>
          );
        }
      }
    
}

function mapStateToProps(state){
  return {
    user : state.User
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({login}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
