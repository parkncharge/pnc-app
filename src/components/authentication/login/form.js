import React, { Component } from "react";
import { View, Keyboard } from "react-native";
import {  Form, Item, Input, Label, Icon } from 'native-base';

import { validateEmail, validatePassword } from '../../../utils/validators';

class LoginForm extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password:'',
            loginParamsValid:false,
            emailValid:false,
            emailError:false,
            emailIcon:null,
            passwordValid:false,
            passwordError:false,
            passwordIcon:null
        };
      }

      inputs = {}; // used to focus text inputs

      focusTheField = (id) => {
        this.inputs[id]._root.focus();
      }

      onEmailChange = (email) => {
        this.setState({ email: email });
        this.updateEmailUI(validateEmail(email), function(){
          this.validateForm();
        });
      };

      onPasswordChange = (password) => {
        this.setState({ password: password });
        this.updatePasswordUI(password, function(){
          this.validateForm();
        });
      };

      updateEmailUI = (valid, cb) => {
        this.setState({
          emailIcon: valid ? 'checkmark-circle':'close-circle',
          emailError:!valid,
          emailValid:valid
        },
        cb)
      }

      updatePasswordUI = (password, cb) => {
        let valid = validatePassword(password)
        this.setState({
          passwordIcon: valid ? 'checkmark-circle':'close-circle',
          passwordError:!valid,
          passwordValid:valid
        },
        cb)
      }

      validateForm = () => {
        let { emailValid, passwordValid, email, password} = this.state;
        if(emailValid && passwordValid) {
          this.props.enableLoginUser(email, password);
        } else {
          this.props.enableLoginUser(null, null);
        }
      }

      render() {
        let { 
          emailError, 
          emailValid, 
          email, 
          emailIcon, 
          passwordValid,
          passwordError,
          passwordIcon,
          password} = this.state;

          return (
            <View class={{ width:'100%'}}>
              <Form>
                <Item floatingLabel success={emailValid} error={emailError}>
                  <Label>Email</Label>
                  <Input 
                    placeholder="" 
                    value={email}
                    keyboardType="email-address"
                    returnKeyType = {"next"}
                    blurOnSubmit={false}
                    onSubmitEditing={() => { this.focusTheField('passwordInput'); }}
                    onChangeText={(text) => this.onEmailChange(text)}/>

                  { (emailValid || emailError) &&
                    <Icon name={emailIcon}/>
                  }

                </Item>

                <Item floatingLabel success={passwordValid} error={passwordError}>
                  <Label>Password</Label>
                  <Input 
                    getRef={input => { this.inputs['passwordInput'] = input }}
                    placeholder="" 
                    secureTextEntry = {true}
                    value={password}
                    maxLength={14}
                    onSubmitEditing={() => { Keyboard.dismiss(); this.validateForm(); }}
                    onChangeText={(text) => this.onPasswordChange(text)}/>
    
                  { (passwordValid || passwordError) &&
                    <Icon name={passwordIcon}/>
                  }

                </Item>
              </Form>
            </View>
          );
      }
}

export default LoginForm;
