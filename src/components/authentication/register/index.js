import React, { Component } from "react";
import { View, StatusBar, Keyboard} from "react-native";
import { Container, Button, Text, Content, Toast} from "native-base";

import { connect } from 'react-redux';
import { signUp } from '../../../store/actions/user_actions';
import {initialiseVehicles} from '../../../store/actions/vehicle_actions';
import { bindActionCreators } from 'redux';

import RegisterForm from './form';
import styles from "../styles";

class Register extends Component {
    static navigationOptions = {
        title: "Sign Up",
      };

      constructor(props) {
        super(props);
        this.state = {
          email:null,
          password:null,
          enableRegisterUser:false
        }
      } 

      updateRegistrationStatus(props) {
        if(props.user.loggedIn){
          console.log("created user");
          props.initialiseVehicles()
          .then(() => {
            props.navigation.navigate('App');
          })
          .catch((err) => {
            console.log(err);
          })
        }
      }

      enableRegisterUser = (email, password) => {
        let enableRegister = (email !== null && 
                              password !== null);

        this.setState({
          email:email,
          password:password,
          enableRegisterUser:enableRegister
        });
      }

      registerUser = () => {
        Keyboard.dismiss();
        this.props.signUp(this.state)
        .then((response) => {
          this.updateRegistrationStatus(this.props);
        })
        .catch((err) => {
          console.log("ERROR registering user");
        });
      }
      _skip() {
        this.props.navigation.navigate('App');
      }

    render () {
        let { enableRegisterUser, } = this.state;

        return (
            <Container style={styles.container}>
            <StatusBar barStyle="light-content" />
            <Content padder>
              <View class={{marginTop:50, width:'100%'}}>
                <RegisterForm enableRegisterUser={this.enableRegisterUser}/>

              <View style={{left:'5%', width:'90%', marginTop:20}}>
                <Button block disabled={!enableRegisterUser} onPress = {() => this.registerUser()}>
                  <Text>Sign Up</Text>
                </Button>
              </View>  
            </View>

            <View style={{ marginTop:30,  width:'100%'}}>
            <Button bordered info onPress = {() => this._skip()}>
                  <Text style={{textAlign:'right'}}>Skip</Text>
                </Button>
            </View>
            </Content>
          </Container>
        ); 
    }
}

function mapStateToProps(state){
  console.log('mapStateToProps');
  console.log(state.User);
  return {
    user : state.User
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({signUp, initialiseVehicles}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Register);