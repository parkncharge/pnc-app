import React, { Component } from "react";
import { View, Keyboard } from "react-native";
import {  Form, Item, Input, Label, Icon } from 'native-base';

import { validateEmail, validatePassword, validateRepeatPassword } from '../../../utils/validators';

class RegisterForm extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password:'',
            repeatPassword:'',
            loginParamsValid:false,
            emailValid:false,
            emailError:false,
            emailIcon:null,
            passwordValid:false,
            passwordError:false,
            passwordIcon:null
        };
      }

      inputs = {}; // used to focus text inputs

      focusTheField = (id) => {
        this.inputs[id]._root.focus();
      }

      onEmailChange = (email) => {
        this.setState({ email: email });
        this.updateEmailUI(validateEmail(email), function(){
          this.validateForm();
        });
      };

      onPasswordChange = (password) => {
        this.setState({ password: password });
        this.updatePasswordUI(password, this.state.repeatPassword, function(){
          this.validateForm();
        });
      };

      onRepeatPasswordChange = (repeatPassword) => {
        this.setState({ repeatPassword: repeatPassword });
        this.updatePasswordUI(this.state.password, repeatPassword, function(){
          this.validateForm();
        });
      };
      
      updatePasswordUI = (password, repeatPassword, cb) => {
        this.updateFirstPasswordUI(validatePassword(password), function(){
          if(repeatPassword.length > 0){
            this.updateRepeatPasswordUI(validateRepeatPassword(password,repeatPassword), cb)
          } else {
            cb;
          }
        })
      }

      updateEmailUI = (valid, cb) => {
        this.setState({
          emailIcon: valid ? 'checkmark-circle':'close-circle',
          emailError:!valid,
          emailValid:valid
        }, 
        cb)
      }

      updateFirstPasswordUI = (valid, cb) => {
        this.setState({
          passwordIcon: valid ? 'checkmark-circle':'close-circle',
          passwordError:!valid,
          passwordValid:valid
        },
        cb)
      }

      updateRepeatPasswordUI = (valid, cb) => {
        this.setState({
          repeatPasswordIcon: valid ? 'checkmark-circle':'close-circle',
          repeatPasswordError:!valid,
          repeatPasswordValid:valid
        }, 
        cb)
      }

      validateForm = () => {
        let { emailValid, passwordValid, repeatPasswordValid, email, password} = this.state;
        if(emailValid && passwordValid && repeatPasswordValid) {
          this.props.enableRegisterUser(email, password);
        } else {
          this.props.enableRegisterUser(null, null);
        }
      }

      render() {
        let { 
          emailError, 
          emailValid, 
          email, 
          emailIcon, 
          passwordValid,
          passwordError,
          passwordIcon,
          password,
          repeatPassword,
          repeatPasswordValid,
          repeatPasswordError,
          repeatPasswordIcon,} = this.state;

          return (
            <View class={{ width:'100%'}}>
              <Form>
                <Item floatingLabel success={emailValid} error={emailError}>
                  <Label>Email</Label>
                  <Input 
                    placeholder="" 
                    value={email}
                    keyboardType="email-address"
                    returnKeyType = {"next"}
                    blurOnSubmit={false}
                    onSubmitEditing={() => { this.focusTheField('passwordInput'); }}
                    onChangeText={(text) => this.onEmailChange(text)}/>

                  { (emailValid || emailError) &&
                    <Icon name={emailIcon}/>
                  }

                </Item>

                <Item floatingLabel success={passwordValid} error={passwordError}>
                  <Label>Password</Label>
                  <Input 
                    getRef={input => { this.inputs['passwordInput'] = input }}
                    placeholder="" 
                    secureTextEntry = {true}
                    value={password}
                    maxLength={14}
                    returnKeyType = {"next"}
                    blurOnSubmit={false}
                    onSubmitEditing={() => { this.focusTheField('passwordInput2'); }}
                    onChangeText={(text) => this.onPasswordChange(text)}/>

                  { (passwordValid || passwordError) &&
                    <Icon name={passwordIcon}/>
                  }

                </Item>

                <Item floatingLabel success={repeatPasswordValid} error={repeatPasswordError}>
                  <Label>Repeat Password</Label>
                  <Input 
                    getRef={input => { this.inputs['passwordInput2'] = input }}
                    placeholder="" 
                    value={repeatPassword}
                    secureTextEntry = {true}
                    maxLength={14}
                    onSubmitEditing={() => { Keyboard.dismiss(); this.validateForm(); }}
                    onChangeText={(text) => this.onRepeatPasswordChange(text)}/>

                  { (repeatPasswordValid || repeatPasswordError) &&
                    <Icon name={repeatPasswordIcon}/>
                  }

                </Item>
              </Form>
            </View>
          );
      }
}

export default RegisterForm;
