import { createStackNavigator } from 'react-navigation';

import LoadingScreen from '../loadingScreen';
import SignInScreen from './login';
import RegisterScreen from './register';
import ForgotPasswordScreen from './passwordReset';


const rootAuthentication = createStackNavigator({
    SignIn: SignInScreen,
    Register:RegisterScreen,
    ForgotPassword: ForgotPasswordScreen,
  },
  {
    initialRouteName: 'SignIn',
  });

  const AuthenticationNavigator = createStackNavigator({
    Home: rootAuthentication,
    LoadingScreen:LoadingScreen,
  },
  {
    initialRouteName: 'Home',
    mode: 'modal',
    headerMode: 'none',
  });

export default AuthenticationNavigator;