import React, { Component } from "react";
import { ImageBackground, View, StatusBar, AsyncStorage } from "react-native";
import { Container } from "native-base";

import styles from "./styles";

const launchscreenLogo = require("../../assets/logo_ico.png");


class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  };

  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
          <View style={styles.logoContainer}>
          </View>
      </Container>
    );
  }
}


export default AuthLoadingScreen;