const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  logo: {
    width: 200,
    height: 200
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    alignItems: 'stretch',
    width: '100%',
  },
  header: {
    backgroundColor:"#B0C4DE"
  },
  container2: {
    marginTop:20, 
    flex:1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center'
  },
  textButton:{

  },
  loading:{
    backgroundColor:'#fff',
    justifyContent:'center',
    alignItems: 'center',
    width:'100%',
    height:deviceHeight,
    flex:1
  }
};