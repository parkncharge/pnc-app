import Ionicons from '@expo/vector-icons/Ionicons';
import { createStackNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import LocationTabNavigator from './locationTab/navigator';
import ChargingTabNavigator from './chargingTab/navigator';
import ProfileTabNavigator from './profileTab/navigator';

const AppNavigator = createMaterialBottomTabNavigator({
    LocationHome: LocationTabNavigator,
    ChargingHome:ChargingTabNavigator,
    ProfileHome:ProfileTabNavigator,
  }, {
    initialRouteName: 'LocationHome',
    activeTintColor: '#f0edf6',
    inactiveTintColor: '#000',
    barStyle: { 
      backgroundColor: '#4682B4' },
  });


export default AppNavigator;