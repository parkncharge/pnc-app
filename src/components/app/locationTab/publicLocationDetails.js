import React, { Component } from "react";
import { View } from "react-native";
import LocationDetailsForm from './locationDetails';

import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Card,
  CardItem,
  Text,
  Body,
  Left,
  Right
} from "native-base";
import styles from "./styles";

class Basic extends Component {
  constructor() {
    super();
    this.state = {
      place:null
    };
  }

  componentDidMount(){
    this.setState({
      place:this.props.navigation.getParam('place', null)
    });
  }

  render() {
    console.log(this.state.place);
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate('LocationHomeScreen')}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Location</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
            {this.state.place &&
              <LocationDetailsForm chargePoint={this.state.place}/>
            } 
        </Content>
      </Container>
    );
  }
}

export default Basic;
