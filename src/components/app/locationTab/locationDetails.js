import React, { Component } from "react";
import { ScrollView, FlatList, View, Linking, TouchableOpacity } from "react-native";
import { Item, Label, Text, List, Icon, Left, Body, Thumbnail, Right, Button, ListItem } from "native-base";

import openMap from 'react-native-open-maps';

import styles from './styles';

const fivePinIcon = require('../../../assets/five_pin.png');

class LocationDetailsForm extends Component {
    constructor() {
        super();
        this.state = {
            
        };
      }
    
      getDirections() {
        const location = this.props.chargePoint.ChargeDeviceLocation.Address.PostCode.length == 0 ? this.props.chargePoint.ChargeDeviceLocation.Address.Street:this.props.chargePoint.ChargeDeviceLocation.Address.PostCode;
        openMap({ end:location});
      }


    getBuildingNameNumber(address){
    let name = "";
    if(address.BuildingName !== null && address.BuildingName !== ""){
        name = address.BuildingName + " ";
    }
    if(address.BuildingNumber !== null && address.BuildingNumber !== ""){
        name += address.BuildingNumber + " ";
    }
    return name;
    }

    getAddress(address){
        let title = "";
        if(title !== null && title !== ""){
        title = address.Street;
        }
        
    if(title === "" && address.Thoroughfare !== null){
        title = address.Thoroughfare;
    } 
    return this.getBuildingNameNumber(address) + title;
    }

    getPaymentDetails(){
        let fee = "Please check at location";
        let {chargePoint} = this.props;
        
      if(chargePoint.PaymentDetails === null || chargePoint.PaymentDetails === ""){
        if(chargePoint.ParkingFeesDetails !== null && chargePoint.ParkingFeesDetails !== ""){
            fee = chargePoint.ParkingFeesDetails;
        }
      } else {
        fee = chargePoint.PaymentDetails;
      }
      return fee;
    }

    getTitle(chargePoint){
        if(chargePoint.ChargeDeviceName !== null && chargePoint.ChargeDeviceName !== ""){
            return chargePoint.ChargeDeviceName;
        }

    }

    

    renderTitle = () => {
        return (
            <Text style={styles.title}>{this.getTitle(this.props.chargePoint)}</Text>
        )
    }


    renderLocation = () => {
        return (
            <View style={styles.addressBar}>
                <Icon name='ios-pin' />
                <View style={styles.addressView}>
                    <Text style={styles.address}>{this.getAddress(this.props.chargePoint.ChargeDeviceLocation.Address)}</Text>
                    <Text style={styles.address}>{this.props.chargePoint.ChargeDeviceLocation.Address.PostTown}</Text>
                    <Text style={styles.address}>{this.props.chargePoint.ChargeDeviceLocation.Address.PostCode}</Text>
                </View>
                <Icon name='arrow-dropright' onPress={() => {this.getDirections()}}>
                </Icon>
            </View>
        )
    }

    renderOperator = () => {
        return (
            <View style={styles.addressBar}>
                <Icon name='ios-pin' />
                <View style={styles.addressView}>
                    <Text style={styles.address}>{this.props.chargePoint.DeviceOwner.OrganisationName}</Text>
                    <Text style={styles.address}>{this.props.chargePoint.DeviceOwner.TelephoneNo}</Text>
                </View>
                <Icon name='arrow-dropright' onPress={() => {this.goToWebsite()}}>
                </Icon>
            </View>
        )
    }

    renderChargePoint = (connector) => {
        return (
            <ListItem
                style={{ borderBottomWidth: 0 }}>
                <View style={styles.connectorView}>
                    <View style={styles.thumbnailView}>
                        <Thumbnail  small square source={ fivePinIcon }/>
                    </View>
                    
                    <View style={styles.connectorDetailView}>
                        <Text>{connector.ConnectorType}</Text>

                        {this.renderHorizontalItem('mode:', connector.ChargeMode)}
                        {this.renderHorizontalItem('output:', connector.RatedOutputkW + 'kW')}
                    </View>
                </View>
            </ListItem>
        )
    }
    renderChargePoints = () => {
        let {Connector} = this.props.chargePoint;
        return (
            <FlatList
                style={{width:'100%'}}
                data={Connector}
                keyExtractor={item => item.ConnectorId}
                
                renderItem={({ item }) => {
                    return this.renderChargePoint(item)
                }}>
            </FlatList>
        )

    }

    renderItem = (label, text) => {
        return (
            <View style={{paddingTop:10}}>
                <Text style={styles.label}>{label}</Text>
                <Text>{text}</Text>
            </View>
        )
    }

    renderHorizontalItem = (label, text) => {
        return (
            <View style={{flexDirection:'row'}}>
                <Text style={styles.label}>{label}</Text>
                <Text>{text}</Text>
            </View>
        )
    }

    openWebBrowser = () => {
        Linking.canOpenURL(this.props.chargePoint.DeviceOwner.Website).then(supported => {
            if (supported) {
              Linking.openURL(this.props.chargePoint.DeviceOwner.Website);
            }
          });
    }
    render(){
        return(
            <ScrollView style={styles.detailsView}>
                {this.renderTitle()}
                {this.renderLocation()}
                <View>
                    {this.renderItem('Location Type:',this.props.chargePoint.LocationType)}
                    {this.renderItem('Price:',this.getPaymentDetails())}
                    {
                        this.props.PhysicalRestrictionFlag &&
                        this.renderItem('Restrictions:',this.props.chargePoint.PhysicalRestrictionText)
                    }
                    {this.renderItem('Operator:',this.props.chargePoint.DeviceOwner.OrganisationName)}
                    {this.renderItem('Phone:',this.props.chargePoint.DeviceOwner.TelephoneNo)}
                    <TouchableOpacity onPress={this.openWebBrowser}>
                        {this.renderItem('Web:',this.props.chargePoint.DeviceOwner.Website)}
                    </TouchableOpacity>
                    <View style={{marginTop:10}}>
                        <Text style={styles.label}>Charge Points:</Text>
                        {this.renderChargePoints()}
                    </View>
                </View>
            </ScrollView>
        );
    }
}

export default LocationDetailsForm;