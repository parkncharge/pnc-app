import React, {Component} from 'react';
import Ionicons from '@expo/vector-icons/Ionicons';
import { createStackNavigator } from 'react-navigation';

import LocationHomeScreen from './locationHomeScreen';
import LocationScreen2 from './locationScreen2';
import PublicLocationDetails from './publicLocationDetails';

const locationStackNavigator = createStackNavigator({
  LocationHomeScreen:LocationHomeScreen,
  LocationScreen2:{screen: LocationScreen2, navigationOptions: { tabBarVisible:false } },
  PublicLocationDetails:{screen: PublicLocationDetails, navigationOptions: { tabBarVisible:true } }
},
{
  initialRouteName: 'LocationHomeScreen',
  headerMode: "none"
});

locationStackNavigator.navigationOptions = ({ navigation }) => {
  return {
    tabBarVisible: navigation.state.index === 0,
    title: 'Locations',
    tabBarLabel:"Locations",
    tabBarIcon: ({ tintColor }) => (
      <Ionicons name="ios-pin" color={tintColor} size={24}/>
    )
  };
};

export default locationStackNavigator;