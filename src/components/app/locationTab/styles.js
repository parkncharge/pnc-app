export default {
    container: {
      backgroundColor: "#FFF"
    },
    header: {
      backgroundColor:"#B0C4DE"
    },
    location: {
      position:'absolute',
      top:10,
      right:10
    },

    mapContainer: {
      width: '100%',
      height: '100%'
    },

    wrapper: {
      paddingTop: 50,
      flex: 1
    },
  
    modal: {
      justifyContent: 'center',
      alignItems: 'center',
      flex:1,
      flexDirection:'column',
      width:'100%'
    },
  
    text: {
      color: "black",
      fontSize: 22
    },
    navigateButton: {
      position: 'absolute', 
      top: 10,
      right:10,
      fontSize: 40,
      color:'blue'
    },
    listButton: {
      position: 'absolute', 
      top: 0,
      left:10,
      fontSize: 50,
      color:'blue'
    },
    mapButton: {
      fontSize: 35,
      color:'blue'
    },
    listItem:{
      paddingLeft: 25
    },
    detailsView:{
      flex: 1
    },
    title:{
      fontSize:20,
      padding:10,
      textAlign: 'center'
    },
    addressBar:{
      marginLeft:10,
      width:"90%",
      flexDirection:'row',
      justifyContent:'space-around',
      paddingBottom: 10,
    },
    operatorBar:{
      width:"100%",
      flexDirection:'row',
      justifyContent:'space-around',
      paddingBottom: 10,
      paddingTop:10
    },
    addressView:{
      paddingLeft:20,
      paddingRight:20
    },
    address:{
      fontSize:16,
      textAlign: 'left'
    },
    label:{
      textAlign: 'left',
      fontSize:12,
      color:'#2F4F4F',
    },
    thumbnailView:{
      justifyContent:'center',
    },
    connectorDetailView:{
      paddingLeft:3,
      flexDirection:'column',
      justifyContent:'space-around',
    },
    connectorView:{
      flexDirection:'row', 
      marginLeft:-16,
    },
    markerImage:{
      width: 30,
      height: 38,
      marginBottom: 35,
    }
  };