import React, { Component, PureComponent } from 'react';
import { View } from 'react-native';
import { MapView } from 'expo';

import { Container, Content, Text, List, ListItem, Icon, Left, Body, Right, Button } from "native-base";

import styles from "./styles";
import LocationDetailsForm from './locationDetails';

const Marker = MapView.Marker;

export default class ListComponent extends PureComponent {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      isDisabled: false,
      swipeToClose: true,
      place:{}
    };
  }

  getTitle(ChargeDeviceLocation){
    let title = ChargeDeviceLocation.Address.Thoroughfare;
    return title;
  }

  renderMarkers() {
    if(this.props.places === null){
        return;
    } else {
        return this.props.places.map((place, i) => (
            <Marker 
              key={place.ChargeDeviceId}
      
              onPress = {() => {
                this.setState({place:place});
                this.refs.modal1.open();
              }}

              coordinate={{
                    latitude:parseFloat(place.ChargeDeviceLocation.Latitude), 
                    longitude:parseFloat(place.ChargeDeviceLocation.Longitude)
                }}
            >
              
            </Marker>
        ));
    }
  }

  openDetailWindow = (place) => {
    this.props.navigation.navigate("PublicLocationDetails", {place:place});
  }

  calculateDistance = (distance) => {
    return distance.toFixed(1) + "m";
  }

  render() {
    const { places, region } = this.props
    return (
      <View>
        <List
          style={{marginTop:50}}
          dataArray={places}
          renderRow={data => {
            return (
              <ListItem 
                style={styles.listItem}
                onPress = {() => {
                      this.openDetailWindow(data);
                    }}>
                <Body>
                  <Text>{this.getTitle(data.ChargeDeviceLocation)}</Text>
                  <Text>{data.ChargeDeviceLocation.Address.PostTown}</Text>
                  <Text>{data.ChargeDeviceLocation.Address.PostCode}</Text>
                  <Text note>{this.calculateDistance(data.ChargeDeviceLocation.Distance)}</Text>
                </Body>
                <Right>
                  <Icon name="arrow-forward"/>
                </Right>
              </ListItem>
              
            );
        }}/>
        <Button style= {{position:'absolute', top:5, left:10}} light onPress={() => {this.props.showList(false)}}>
            <Icon name="map" style={styles.mapButton} />
        </Button>
        
      </View>
    );
  }
}

