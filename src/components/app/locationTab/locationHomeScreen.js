import React, {Component, PureComponent} from 'react';
import { SafeAreaView, Keyboard, View } from 'react-native';
import { Location, Permissions } from 'expo';
import { debounce } from "debounce";

import { Header, Item, Input, Icon, Button, Text, Container } from 'native-base';
import { connect } from 'react-redux';
import { userInfo, logOut, getUserData } from '../../../store/actions/user_actions';
import { getVehicles } from '../../../store/actions/vehicle_actions';
import { getCards } from '../../../store/actions/card_actions';

import { bindActionCreators } from 'redux';
import Spinner from 'react-native-loading-spinner-overlay';

import { toastr, getPNCData } from "../../../utils/misc";
import {distanceByRoad} from '../../../utils/geoCalcs';

import Map from "./map";
import ListComponent from "./list";
import styles from "./styles";

const deltas = {
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421
};

//var screen = Dimensions.get('window');

class LocationHomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      region:null,
      location: null,
      pncPoints:[],
      errorMessage: null,
      showCancel:false,
      searchQuery:"",
      showList:false,
      gotAccountInfo:false
    };
  }

  componentDidMount() {
    if(!this.state.gotAccountInfo && this.props.user.loggedIn){
      this.updateUserAccountInfo();
    }
  }

  updateUserAccountInfo = () => {
    this.props.getUserData(this.props.user)
    
    .then((response) => {
      this.props.getVehicles(this.props.user);
    })
    
    .then((response) =>{
      this.props.getCards(this.props.user)
      })

    .then((response) => {
        console.log(this.props.user);
    })

    .catch(function(err){
      console.log(err);
    });
  }

  getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied'
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    const region = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
      ...deltas
    };
    this.setState({region:region});
  }

  getLocations = async (region) => {
    this.setState({region:region});
    let PNCLocationsAsync = this.getPNCLocations(region);
    let PNCLocations = await PNCLocationsAsync;
    //console.log(PNCLocations);

    this.setState({ pncPoints:PNCLocations });
  }

  getPNCLocations = async (region) => {
    console.log('Getting PNC locations');
    
    try{
      let pncPoints = await getPNCData(region);
      return this.updateChargePointsWithDistance(pncPoints, region);
    }
    catch(err){
      console.log(err);
      return [];
    }
  }

 

  updateChargePointsWithDistance = (chargePoints, currentLocation) => {
    if(chargePoints !== null){
      if(chargePoints.length > 0){
        let cp = chargePoints.map(function(chargePoint){
          chargePoint.ChargeDeviceLocation.Distance = distanceByRoad(
            currentLocation.latitude, 
            currentLocation.longitude,
            chargePoint.ChargeDeviceLocation.Latitude,
            chargePoint.ChargeDeviceLocation.Longitude
          );
          return chargePoint;
        });
        cp.sort(function(a,b){
          return a.ChargeDeviceLocation.Distance > b.ChargeDeviceLocation.Distance
        });
        return cp;
      } else {
        return [];
      }
    }
  }


  onRegionChanged = (region) => {
    console.log('region changed');
    debounce(this.getLocations(region), 500, true);
  }

  cancelSearch = () => {
    if(this.state.showCancel){
      this.setState({showCancel:false, searchQuery:""});
      Keyboard.dismiss();
    }
  }
 
  searchForLocation = () => {
    //console.log(this.state.searchQuery);
    Location.geocodeAsync(this.state.searchQuery)
      .then((locations) => {
        //console.log(locations);
        if(locations.length > 0) {
          const region = {
            latitude: locations[0].latitude,
            longitude: locations[0].longitude,
            ...deltas
          }
          this.setState({region:region, showCancel:false, searchQuery:""});
          Keyboard.dismiss();
        } else {
          toastr.showToast("Sorry, we couldn't find that location");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  recenter = () => {
    console.log('recenter map');
    this.getLocationAsync();
  }

  renderCancel = () => {
    if (this.state.showCancel) {
        return (
          <Button transparent onPress={() => this.cancelSearch()}>
            <Text>Cancel</Text>
          </Button>
        );
    } else {
        return null;
    }
}

  showList = (state) => {
    //console.log('showList: ' + state);
    this.setState({showList:state})
  }

  renderList = () => {
    let {
      region,
      pncPoints
    } = this.state;

    return (
      <ListComponent 
          region={region} 
          places={pncPoints} 
          recenter={this.recenter} 
          showList={this.showList} 
          navigation={this.props.navigation}
          regionChanged={this.onRegionChanged}/>
    );
  }

  renderMap = () => {
    let {
      region,
      pncPoints
    } = this.state;

    return (
      <Map 
          region={region} 
          places={pncPoints} 
          recenter={this.recenter} 
          showList={this.showList} 
          navigation={this.props.navigation}
          regionChanged={this.onRegionChanged}/>
    );
  }

  renderView = () => {
    if(this.state.showList){
      return this.renderList()
    } else{
      return this.renderMap()
    }
  }

 isFetching = () => {
    return this.props.vehicles.isFetching || this.props.cards.isFetching;
 }

  render() {
    let {
      searchQuery
    } = this.state;

    return (
      <Container style={styles.container}>    
        <Header searchBar style={styles.header}>
          <Item>
            <Icon name="ios-search" />
            <Input 
            ref={'searchBar'}
              placeholder="Search" 
              value={searchQuery}
              onFocus={() => this.setState({showCancel:true})}
              onChangeText={(text) => this.setState({ searchQuery: text })}
              returnKeyType = {"search"}
              blurOnSubmit={true}
              onSubmitEditing={(text) => { this.searchForLocation() }}
            />
          </Item>
          {this.renderCancel()}
        </Header>
        <Spinner
          visible= { this.isFetching() }
          textContent={''}
          textStyle={styles.spinnerTextStyle}
        />
        {this.renderView()}
        

        
        
      </Container>
    );
  }
}

function mapStateToProps(state){
  return {
    user : state.User,
    vehicles: state.Vehicles,
    cards: state.Cards
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({userInfo, logOut, getUserData, getVehicles, getCards}, dispatch);
}



export default connect(mapStateToProps, mapDispatchToProps)(LocationHomeScreen)