import React, { Component, PureComponent } from 'react';
import { View, Image } from 'react-native';
import { MapView } from 'expo';

import {
  Button,
  Icon,
  Text,
} from "native-base";
import styles from "./styles";

const Marker = MapView.Marker;
const markerImg = require('../../../assets/elec_blue.png');
const PNCMarkerImg = require('../../../assets/elec_purple.png');

export default class Map extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      isDisabled: false,
      swipeToClose: true,
      place:{}
    };
  }
  
  shouldComponentUpdate(nextProps, nextState){
    var shouldUpdate = true;
    /*
    if(this.props.region !== null){
      if(this.props.region.latitude === nextProps.region.latitude){
        shouldUpdate = false;
      }
    }
    */
    //console.log('shouldUpdate: ' + shouldUpdate);
    return shouldUpdate;
  }

  getTitle(ChargeDeviceLocation){
      title = ChargeDeviceLocation.LocationShortDescription;
      if(title === null || title =="") {
          title = ChargeDeviceLocation.LocationLongDescription;
          if(title === null || title =="") {
            title = ChargeDeviceLocation.Address.Thoroughfare;
            if(title === null || title =="") {
              title = ChargeDeviceLocation.Address.PostCode;
            } else {
              if(ChargeDeviceLocation.Address.BuildingNumber !== ""){
                title = ChargeDeviceLocation.Address.BuildingNumber + " " + title;
              }
            }
          }
      }

      if(title === ""){ 
        console.log('NO TITLE');
        console.log(ChargeDeviceLocation);
      }
      return title;
  }

  openDetailWindow = (place) => {
    this.props.navigation.navigate("PublicLocationDetails", {place:place});
  }

  getMarkerImage = (place) => {
    if(place.DeviceController.SchemeCode === "PNC"){
      return PNCMarkerImg;
    } else {
      return markerImg;
    }
  }

  renderMarkers() {
    if(this.props.places === null){
        return;
    } else {
        return this.props.places.map((place, i) => (
            <Marker 
              key={place.ChargeDeviceId}
              tracksViewChanges={true}
              onPress = {() => {
                this.openDetailWindow(place);
              }}

              coordinate={{
                    latitude:parseFloat(place.ChargeDeviceLocation.Latitude), 
                    longitude:parseFloat(place.ChargeDeviceLocation.Longitude)
                }}
            >
              <Image
                source={this.getMarkerImage(place)}
                style={styles.markerImage}
              />
            </Marker>
        ));
    }
  }

 

  render() {
    const { region } = this.props

    return (
      <View>
        <MapView
        style={styles.mapContainer}
        region={region}
        onRegionChangeComplete  = {this.props.regionChanged}
        showsUserLocation
        showsMyLocationButton
        >
          {this.renderMarkers()}
          <Button onPress={() => {this.props.showList(true)}}>
            <Icon name="list" style={styles.listButton} />
          </Button>
          
          <Icon name="navigate" style={styles.navigateButton} onPress={() => {this.props.recenter()}}/>
        </MapView>
      </View>
    );
  }
}

