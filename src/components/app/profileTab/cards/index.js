import React, { Component } from "react";
import { Keyboard } from "react-native";
import {addCard, deleteCard} from '../../../../store/actions/card_actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Spinner from 'react-native-loading-spinner-overlay';

import { View, ListView } from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Body,
  Left,
  Right
} from "native-base";
import styles from "../styles";

class Cards extends Component {

  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      basic: true,
      swipeToClose: true,
    };
  }

  componentDidMount() {
    console.log('cards');
    console.log(this.props.cards);
   }

  deleteRow(secId, rowId, rowMap, data) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    this.props.deleteCard(data, rowId);
  }

  renderList = () => {
    return (
      <View style={{marginTop:2, width:'100%'}}>
        <List
          rightOpenValue={-75}
          
          dataSource={this.ds.cloneWithRows(this.props.cards.registeredCards)}

          renderRightHiddenRow={(data, secId, rowId, rowMap) => 
            <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap, data)}>
              <Icon active name="trash" />
            </Button>}

          renderRow={card => {
            return (
              <ListItem style={styles.listItem}>
                <Body>
                  <Text>**** **** **** {card.last4Digits}</Text>
                </Body>
              </ListItem>
            );
          }}
        
        />
      </View>
    )
  }

  renderInstructions = () => {
    return (
      <View style={styles.informationView}>
        <Text style={styles.informationText}>
          You register your card the first time you use our service.
        </Text>
      </View>
    )
  }

  renderView = () => {
    if(this.props.cards.registeredCards.length > 0){
      return this.renderList();
    } else {
      return this.renderInstructions();
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <Spinner
          visible={this.props.cards.isFetching}
          textContent={''}
          textStyle={styles.spinnerTextStyle}
        />

        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate('ProfileHomeScreen')}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Cards</Title>
          </Body>
          <Right></Right>
        </Header>
        
        {this.renderView()}
        
      </Container>
    );
  }
}

function mapStateToProps(state){
  return {
    user : state.User,
    cards: state.Cards
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({addCard, deleteCard}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Cards);
