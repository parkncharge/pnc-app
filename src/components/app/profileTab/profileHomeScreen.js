import React, {Component} from 'react';
import { StyleSheet, View, AsyncStorage } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';

import { connect } from 'react-redux';
import { userInfo, logOut } from '../../../store/actions/user_actions';
import { bindActionCreators } from 'redux';
import styles from "./styles";

import { Container, Header, Body, Content, List, Title, ListItem, Text, Left, Right, Icon, Button } from 'native-base';

class ListItemNoIndentExample extends Component {

  navigateTo = (screen) => {
    this.props.navigation.navigate(screen);
  };

  logoff = () => {
    console.log('logout');
    this.props.logOut().then(() => {
      this.navigateTo('AuthLoading');
    });
  }

  getLoginText = () => {
    return this.props.user.loggedIn?"Logout":"Login";
  }
  render() {
    console.log(this.props.user);
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left/>
          <Body>
            <Title>Profile</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <List>
            <ListItem onPress={() => this.navigateTo('VehiclesScreen')}>
              <Left>
                <Text>My Vehicles</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward"/>
              </Right>
            </ListItem>
            <ListItem onPress={() => this.navigateTo('CardsScreen')}>
              <Left>
                <Text>My Cards</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward"/>
              </Right>
            </ListItem>
            <ListItem onPress={() => this.navigateTo('SettingsScreen')} >
             <Left>
                <Text>Settings</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward"/>
              </Right>
            </ListItem>

            <ListItem>
              <Body>
                <Button full danger onPress={this.logoff}>
                  <Text>{this.getLoginText()}</Text>
                </Button>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}


function mapStateToProps(state){
  return {
    user : state.User
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({userInfo, logOut}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListItemNoIndentExample)