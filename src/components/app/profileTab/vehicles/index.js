import React, { Component } from "react";
import { Keyboard } from "react-native";
import {addVehicle, deleteVehicle} from '../../../../store/actions/vehicle_actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Spinner from 'react-native-loading-spinner-overlay';
import Modal from 'react-native-modalbox';
import VehicleForm from './vehicleForm';

import { View, ListView } from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Body,
  Left,
  Right
} from "native-base";
import styles from "../styles";

class Vehicles extends Component {

  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      basic: true,
      regNo:null,
      make:null,
      model:false,
      enableAddVehicle:false,
      swipeToClose: true,
    };
  }

  componentDidMount() {
    console.log('vehicles');
   }

  vehicleDetails = (vehicle) => {
    return vehicle.make + ' ' + vehicle.model;
  }

  deleteRow(secId, rowId, rowMap, data) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    this.props.deleteVehicle(data, rowId);
  }

  enableAddVehicle = (regNo, make, model) => {
    let enableAdd = (regNo !== null && make !== null && model !== null);

    this.setState({
        regNo:regNo,
        make:make,
        model:model,
        enableAddVehicle:enableAdd
    });
  }

  addVehicle = () => {
    Keyboard.dismiss();
    const data = {
      "vehicleId":null,
      "userId": this.props.user.userData.uid,
      "regNo": this.state.regNo,
      "data":{
    	  "make":this.state.make,
    	  "model":this.state.model
      }
    }
    this.props.addVehicle(data)
    .then((response) => {
        this.closeModal();
    })
    .catch((err) => {
      console.log(err);
      this.closeModal();
    })
  }

  renderModal = () => {
    let { enableAddVehicle, } = this.state;
    return (
      <Modal
        ref={"modal1"}
        swipeToClose={this.state.swipeToClose}
        onClosed={this.onClose}
        onOpened={this.onOpen}
        onClosingState={this.onClosingState}>

          <VehicleForm enableAddVehicle={this.enableAddVehicle} closeModal={this.closeModal}/>
          <View style={{left:'5%', width:'90%', marginTop:20}}>
              <Button block disabled={!enableAddVehicle} onPress = {() => this.addVehicle()}>
              <Text>Add Vehicle</Text>
              </Button>
          </View>  
      </Modal>
    );
  }

  renderList = () => {
    return (
      <View style={{marginTop:2, width:'100%'}}>
        <List
          rightOpenValue={-75}
          
          dataSource={this.ds.cloneWithRows(this.props.vehicles.registeredVehicles)}

          renderRightHiddenRow={(data, secId, rowId, rowMap) => 
            <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap, data)}>
              <Icon active name="trash" />
            </Button>}

          renderRow={vehicle => {
            return (
              <ListItem style={styles.listItem}>
                <Body>
                  <Text>{vehicle.regNo}</Text>
                  <Text note>{this.vehicleDetails(vehicle.data)}</Text>
                </Body>
              </ListItem>
            );
          }}
        
        />
      </View>
    )
  }

  renderInstructions = () => {
    return (
      <View style={styles.informationView}>
        <Text style={styles.informationText}>
          You won't be able to charge your vehicle at any of our charging stations without registering the vehicle you want to charge.
        </Text>
        <Text style={styles.informationText}>
          Click on the  + button to add a vehicle.
        </Text>
      </View>
    )
  }

  renderView = () => {
    if(this.props.vehicles.registeredVehicles.length > 0){
      return this.renderList();
    } else {
      return this.renderInstructions();
    }
  }

  openModal = () => {
    this.refs.modal1.open();
  }

  closeModal = () => {
    this.refs.modal1.close();
  }

  render() {
    return (
      <Container style={styles.container}>
        <Spinner
          visible={this.props.vehicles.isFetching}
          textContent={''}
          textStyle={styles.spinnerTextStyle}
        />
        {this.renderModal()}
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate('ProfileHomeScreen')}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Vehicles</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.openModal()}>
              <Icon name="add" />
            </Button>
          </Right>
        </Header>
        
        {this.renderView()}
        
       
      </Container>
    );
  }
}

function mapStateToProps(state){
  return {
    user : state.User,
    vehicles: state.Vehicles
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({addVehicle, deleteVehicle}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Vehicles);
