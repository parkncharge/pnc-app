import React, { Component } from "react";
import { View, Keyboard } from "react-native";
import {  Form, Item, Input, Label, Icon, Header, Left, Right, Text, Button, Body, Title } from 'native-base';

class AddVehicleForm extends Component {
    constructor() {
        super();
        this.state = {
            regNo: '',
            make:'',
            model:'',
            regNoValid:false,
            makeValid:false,
            modelValid:false,
            regNoError:false,
            makeError:false,
            modelError:false,
            regNoIcon:null,
            makeIcon:null,
            modelIcon:null
        };
    }

    inputs = {}; // used to focus text inputs

    focusTheField = (id) => {
    this.inputs[id]._root.focus();
    }

    validateString = (regNo, length) => {
        return regNo.length > length;
    }

    onRegNoChange = (regNo) => {
        this.setState({ regNo: regNo });
        this.updateRegNoUI(this.validateString(regNo, 4), function(){
          this.validateForm();
        });
      };

    onMakeChange = (make) => {
        this.setState({ make: make });
        this.updateMakeUI(this.validateString(make, 0), function(){
            this.validateForm();
        });
    };

    onModelChange = (model) => {
        this.setState({ model: model });
        this.updateModelUI(this.validateString(model, 0), function(){
            this.validateForm();
        });
    };

    updateRegNoUI = (valid, cb) => {
        this.setState({
            regNoIcon: valid ? 'checkmark-circle':'close-circle',
            regNoError:!valid,
            regNoValid:valid
        }, 
        cb)
    }

    updateMakeUI = (valid, cb) => {
        this.setState({
            makeIcon: valid ? 'checkmark-circle':'close-circle',
            makeError:!valid,
            makeValid:valid
        }, 
        cb)
    }

    updateModelUI = (valid, cb) => {
        this.setState({
            modelIcon: valid ? 'checkmark-circle':'close-circle',
            modelError:!valid,
            modelValid:valid
        }, 
        cb)
    }

    validateForm = () => {
    let { regNoValid, makeValid, modelValid, regNo, make, model} = this.state;
        if(regNoValid && makeValid && modelValid) {
            this.props.enableAddVehicle(regNo, make, model);
        } else {
            this.props.enableAddVehicle(null, null, null);
        }
    }
    
    render() {
        let {
            regNo, 
            regNoValid, 
            regNoError, 
            regNoIcon,
            make, 
            makeValid, 
            makeError, 
            makeIcon,
            model, 
            modelValid, 
            modelError, 
            modelIcon
        } = this.state;
        return (
            <View class={{ width:'100%'}}>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.closeModal()}>
                    <Text>Cancel</Text>
                    </Button>
                </Left>
                <Body>
                    <Title>Add Vehicle</Title>
                </Body>
                <Right/>
            </Header>
              <Form>
                <Item floatingLabel success={regNoValid} error={regNoError}>
                    <Label>Reg No</Label>
                    <Input 
                        placeholder="" 
                        value={regNo}
                        returnKeyType = {"next"}
                        blurOnSubmit={false}
                        onSubmitEditing={() => { this.focusTheField('make'); }}
                        onChangeText={(text) => this.onRegNoChange(text)}/>

                    { (regNoValid || regNoError) &&
                        <Icon name={regNoIcon}/>
                    }
                </Item>

                <Item floatingLabel success={makeValid} error={makeError}>
                    <Label>Make</Label>
                    <Input 
                        getRef={input => { this.inputs['make'] = input }}
                        placeholder="" 
                        value={make}
                        returnKeyType = {"next"}
                        blurOnSubmit={false}
                        onSubmitEditing={() => { this.focusTheField('model'); }}
                        onChangeText={(text) => this.onMakeChange(text)}/>

                    { (makeValid || makeError) &&
                        <Icon name={makeIcon}/>
                    }
                </Item>

                <Item floatingLabel success={modelValid} error={modelError}>
                    <Label>Model</Label>
                    <Input 
                        getRef={input => { this.inputs['model'] = input }}
                        placeholder="" 
                        value={model}
                        returnKeyType = {"done"}
                        blurOnSubmit={false}
                        onSubmitEditing={() => {
                            console.log('pressed DONE')
                            this.validateForm();
                            setTimeout(
                                function(){
                                    Keyboard.dismiss();
                                },
                                50
                            );
                        }}
                        onChangeText={(text) => this.onModelChange(text)}/>

                    { (modelValid || modelError) &&
                        <Icon name={modelIcon}/>
                    }
                </Item>
              </Form>
            </View>
        );
    }
}

export default AddVehicleForm;