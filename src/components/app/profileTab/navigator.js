import React, {Component} from 'react';
import Ionicons from '@expo/vector-icons/Ionicons';
import { createStackNavigator } from 'react-navigation';

import ProfileHomeScreen from './profileHomeScreen';
import SettingsScreen from './settings';
import VehiclesScreen from './vehicles';
import CardsScreen from './cards';
import ListChargePointsScreen from './chargePoints';

const profileStackNavigator = createStackNavigator({
  ProfileHomeScreen:ProfileHomeScreen,
  SettingsScreen:{screen: SettingsScreen, navigationOptions: { tabBarVisible:false } },
  VehiclesScreen:{screen: VehiclesScreen, navigationOptions: { tabBarVisible:false } },
  CardsScreen:{screen: CardsScreen, navigationOptions: { tabBarVisible:false } },
  ListChargePointsScreen:{screen: ListChargePointsScreen, navigationOptions: { tabBarVisible:false } },
},
{
  initialRouteName: 'ProfileHomeScreen',
  headerMode: "none"
});

profileStackNavigator.navigationOptions = ({ navigation }) => {
  return {
    tabBarVisible: navigation.state.index === 0,
    title: 'Profile',
    tabBarLabel:"Profile",
    tabBarIcon: ({ tintColor }) => (
      <Ionicons name="ios-list" color={tintColor} size={24}/>
    )
  };
};

export default profileStackNavigator;