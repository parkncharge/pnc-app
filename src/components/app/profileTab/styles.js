export default {
  container: {
    backgroundColor: "#FFF"
  },
    header: {
      backgroundColor:"#B0C4DE"
    },
    view: {
      padding:10
    },
    button:{
      backgroundColor: "#6FAF98", 
      alignSelf: "center"
    },
    list:{
      marginTop:100
    },
    listItem:{
      height:50,
      
    },
    informationView:{
      flex:1,
      height:200,
      top:60,
    },
    informationText:{
      padding:20,
    },
    label:{
      font:12,
    }
  };