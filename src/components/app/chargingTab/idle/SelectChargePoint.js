import React, { PureComponent } from "react";
import { View, StyleSheet, Animated, Easing } from "react-native";
import {
  Button,
  Text
} from "native-base";
import styles from "../styles";

import { connect } from 'react-redux';
import { userInfo, getUserData, updateUserData } from '../../../../store/actions/user_actions';
import { getChargePointStatus } from '../../../../store/actions/chargePointStatus_actions';
import { bindActionCreators } from 'redux';

import { Permissions, Camera } from 'expo';

/*
Permissable states:
  "fault"
  "available"
  "waitingForAuth"
  "authorised"
  "charging"
  "complete" 
*/

class SelectChargePointUI extends PureComponent {
  constructor() {
    super();
    this.animatedValue = new Animated.Value(0);
    this.state = {
      hasCameraPermission: null,
      scanning:false,
      scanned: true
    }
  }
 
  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
    /*
    console.log("componentDidMount");
    console.log(this.props.user);
    console.log(this.props.chargePointStatus);
    console.log("================");
    */
  }

  componentDidUpdate() {
    /*
    console.log('component did update');
    //console.log(this.props.chargePointStatus);
    console.log("=============");
    */
  }
 
  animate = (start) => {
    this.animatedValue.setValue(0)
    Animated.timing(
      this.animatedValue,
      {
        toValue: 1,
        duration: 2000,
        easing: Easing.linear
      }
    ).start(() => {
      if(this.state.scanning || start){
        this.animate(false);
      }
    });
  };

handleBarCodeScanned = async ({ type, data }) => {
  if(!this.state.scanned){
    this.animatedValue.stopAnimation();
    this.setState({ scanned: true, scanning:false});
    console.log(`Bar code with type ${type} and data ${data} has been scanned!`);
    if(type === "org.iso.QRCode") {
      let arr = data.split("|");
      if(arr.length === 2){
        console.log('getting status');
        this.props.getChargePointStatus(arr[0], arr[1])
        .catch((err) => {
          console.log("ERROR in getChargePointStatus");
        });
      } else {
        alert("Sorry, this isn't a recognised QR code");
      }
    } else {
        alert("Sorry, this isn't a recognised QR code");
    }
  }
};


  scan = () => {
    if(!this.state.scanning){
      this.setState({scanned:false, scanning:true});
      this.animate(true);
    }
  }

  render() {
    const { hasCameraPermission, scanning, scanned } = this.state;

    const movingMargin = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 240, 0]
    });

    if (hasCameraPermission === null) {
      return null;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }

    return (
        <View style={{flex: 1, justifyContent: 'space-evenly', flexDirection: 'column' }}>
          <View style={{paddingTop:30, height:150}}>
            <Text style={{ textAlign: 'center'}}>Scan the QR code on the Charge Point connector</Text>
          </View>
          <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
            <View style={{width: 250, height: 250, borderColor: 'orange', borderWidth:2, borderRadius:5}}>
              <Camera
                  onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
                  style={StyleSheet.absoluteFillObject}
              />
              <Animated.View
                style={{
                  marginTop: movingMargin,
                  height: 2,
                  width: 246,
                  backgroundColor: 'red'}} 
              />
            </View>
          </View>
          
          <View style={{height: 100}}>
            <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
              <Button disabled={this.state.scanning} onPress = {this.scan}>
                <Text>Scan</Text>
              </Button>
            </View>
          </View>

          
        </View>
          
     
    );
  }
}

function mapStateToProps(state){
  return {
    user : state.User,
    chargePointStatus : state.ChargePointStatus
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({getChargePointStatus}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectChargePointUI);
