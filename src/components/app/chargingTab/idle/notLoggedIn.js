import React, { Component } from "react";
import { View } from "react-native";
import {
  Content,
  Card,
  CardItem,
  Text,
  Body,
} from "native-base";
import styles from "../styles";

class NotLoggedIn extends Component {
  render() {
    return (
        <Content padder>
          <Card style={styles.mb}>
            <CardItem>
              <Body>
                <Text>Please register or log in</Text>
                <Text>You must register or log in before you can charge your vehicle. It only takes a minute!</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
    );
  }
}

export default NotLoggedIn;
