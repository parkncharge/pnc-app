import React, { Component } from "react";
import { View } from "react-native";
import {
  Content,
  Card,
  CardItem,
  Text,
  Body,
} from "native-base";
import styles from "../styles";

class ChargePointUnavailable extends Component {
  render() {
    return (
        <Content padder>
          <Card style={styles.mb}>
            <CardItem>
              <Body>
                <Text>We're sorry</Text>
                <Text>This Charge Point is unavailable</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
    );
  }
}

export default ChargePointUnavailable;
