import React, { Component } from "react";
import { View, ScrollView, TouchableOpacity, StyleSheet, Picker } from "react-native";
import {
  Content,
  Card,
  CardItem,
  Text,
  Body,
} from "native-base";
import styles from "../styles"






class WaitingForAuthStateUI extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language:'js'
    }
  }

  getTitle(chargePoint){
    if(chargePoint.ChargeDeviceName !== null && chargePoint.ChargeDeviceName !== ""){
        return chargePoint.ChargeDeviceName;
    }
  }

  renderTitle = () => {
    return (
        <Text style={styles.title}>{this.getTitle(this.props.chargePoint)}</Text>
    )
  }

  renderItem = (label, text) => {
    return (
        <View style={{paddingTop:10}}>
            <Text style={styles.label}>{label}</Text>
            <Text>{text}</Text>
        </View>
    )
  }

  getPaymentDetails(){
    let fee = "Please check at location";
    let {chargePoint} = this.props;
    
    if(chargePoint.PaymentDetails === null || chargePoint.PaymentDetails === ""){
      if(chargePoint.ParkingFeesDetails !== null && chargePoint.ParkingFeesDetails !== ""){
          fee = chargePoint.ParkingFeesDetails;
      }
    } else {
      fee = chargePoint.PaymentDetails;
    }
    return fee;
  }

  

  selectDuration = () => {
    return (
      <View style={{paddingTop:10}}>
            <Text style={styles.label}>SelectDuration:</Text>
            <Picker
              selectedValue={this.state.language}
              style={{ height: 50, width: 100 }}
              onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
              <Picker.Item label="Java" value="java" />
              <Picker.Item label="JavaScript" value="js" />
            </Picker>
      </View>

      
    );
  }

  selectPower = () => {
    return null;
  }

  displayCardInfo = () => {
    return null;
  }

  render() {
    const placeholder = {
      label: 'Select a sport...',
      value: null,
      color: '#9EA0A4',
    };

    return (
        <Content padder>
          <ScrollView style={styles.detailsView}>
            {this.renderTitle()}
            <View>
                {this.renderItem('Price:',this.getPaymentDetails())}
                {this.selectDuration()}
                {this.selectPower()}
                {this.displayCardInfo()}
            </View>
          </ScrollView>
        </Content>
    );
  }

  
}



export default WaitingForAuthStateUI;
