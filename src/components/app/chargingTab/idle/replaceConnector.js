import React, { Component, PureComponent } from "react";
import { View } from "react-native";
import {
  Content,
  Card,
  CardItem,
  Text,
  Body,
} from "native-base";
import styles from "../styles";

class ReplaceConnector extends PureComponent {
  constructor() {
    super();
    this.timer = null;
    this.state = {
      allowPolling:true
    };
  }

  componentDidMount() {
    this.pollTimer();
  }
  
  componentWillUnmount = () => {
    console.log('unmounting');
    this.setState({allowPolling:false});
    clearTimeout(this.timer);
  };
  

  processTimeout = () => {
    console.log('processing timeout');
    if(this.state.allowPolling){
      this.pollTimer();
    }
  }
  
  pollTimer = () => {
    this.timer = setTimeout(
      this.processTimeout(),
      5000
    );
  }

  


  
  render() {
    return (
        <Content padder>
          <Card style={styles.mb}>
            <CardItem>
              <Body>
                <Text>Please replace the connector securely into the Charge Point if it is not in use.</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
    );
  }
}

export default ReplaceConnector;
