import React, { Component } from "react";
import { View } from "react-native";
import {
  Content,
  Card,
  CardItem,
  Text,
  Body,
} from "native-base";
import styles from "../styles";

class AuthorisedStateUI extends Component {
  render() {
    return (
        <Content padder>
          <Card style={styles.mb}>
            <CardItem>
              <Body>
                <Text>AuthorisedStateUI</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
    );
  }
}

export default AuthorisedStateUI;
