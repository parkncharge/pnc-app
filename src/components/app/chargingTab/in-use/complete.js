import React, { Component } from "react";
import { View } from "react-native";
import {
  Content,
  Card,
  CardItem,
  Text,
  Body,
} from "native-base";
import styles from "../styles";

class ChargingCompleteStateUI extends Component {
  render() {
    return (
        <Content padder>
          <Card style={styles.mb}>
            <CardItem>
              <Body>
                <Text>ChargingCompleteStateUI</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
    );
  }
}

export default ChargingCompleteStateUI;
