import React, { Component } from "react";
import { View } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Card,
  CardItem,
  Text,
  Body,
  Left,
  Right
} from "native-base";
import styles from "./styles";

import { connect } from 'react-redux';
import { userInfo, getUserData, updateUserData } from '../../../store/actions/user_actions';
import { initialiseChargePointStatus, getChargePointStatus, updateChargePointStatus } from '../../../store/actions/chargePointStatus_actions';
import { bindActionCreators } from 'redux';

import NotLoggedInUI from "./idle/notLoggedIn";
import SelectChargePointUI from "./idle/SelectChargePoint";
import WaitingForAuthStateUI from "./idle/waitingForAuth";
import ReplaceConnectorUI from "./idle/replaceConnector";

import ChargingFaultStateUI from "./in-use/fault";
import AvailableStateUI from "./in-use/available";
import AuthorisedStateUI from "./in-use/authorised";
import ChargingStateUI from "./in-use/charging";
import ChargingCompleteStateUI from "./in-use/complete";

import Spinner from 'react-native-loading-spinner-overlay';
import { withNavigation } from "react-navigation";

/*
Permissable states:
  "fault"
  "available"
  "waitingForAuth"
  "authorised"
  "charging"
  "complete" 
*/

class ChargingHomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      chargePointStateUpdated:false,
      showSpinner:false
    }
  }

  componentDidMount() {
    const { navigation } = this.props;

    console.log('component did mount');
    console.log(this.props.chargePointStatus);
    console.log("=============");


    if(this.props.user.loggedIn){
      //this.props.initialiseChargePointStatus();
      this.focusListener = navigation.addListener("didFocus", () => {
        console.log('screen got focus');
        if(!this.props.user.appState.state === "idle") {
          this.props.getChargePointStatus(
            this.props.user.appState.chargePointId, 
            this.props.user.appState.connectorId);
        }
      });
    }
  }

  componentWillUnmount() {
    // Remove the event listener
    this.focusListener.remove();
  }

  
  componentDidUpdate() {
    
    console.log('component did update');
    console.log(this.props.chargePointStatus);
    console.log("=============");
    
    if(this.state.showSpinner && this.props.chargePointStatus.isFetching){
      this.setState({showSpinner:false});
    }
  }


  isFetching = () => {
    return this.props.chargePointStatus.isFetching || this.state.showSpinner;
  }

  
  renderIdleComponent = () => {
    if(this.props.chargePointStatus.chargingData === null){
      return (
        <SelectChargePointUI/>
      )
    } else {
      switch(this.props.chargePointStatus.chargingData.Status){
        case "fault":
          return (<ChargingFaultStateUI/>);
        case "available":
          return (<WaitingForAuthStateUI chargePoint={this.props.chargePointStatus.chargingData.ChargePoint}/>);
        default:
          return (<ReplaceConnectorUI/>);
      }
    }
  }

  renderInUseComponent = () => {
    console.log(this.props.chargePointStatus.chargingData.Status);
    switch(this.props.chargePointStatus.chargingData.Status){
      case "fault" :
        return (<ChargingFaultStateUI/>);
      case "available" :
        return (<AvailableStateUI/>);
      case "waitingForAuth" :
        return (<WaitingForAuthStateUI/>);
      case "authorised" :
        return (<AuthorisedStateUI/>);
      case "charging" :
        return (<ChargingStateUI/>);
      case "complete" :
        return (<ChargingCompleteStateUI/>);
    }

  }

  renderComponent = () => {
    if(!this.props.user.loggedIn){
      return (
        <NotLoggedInUI/>
      )
    } else {
      if(this.props.user.appState.state === "idle"){
        return this.renderIdleComponent();
      } else {
        return this.renderInUseComponent();
      }
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left/>
          <Body>
            <Title>Charging</Title>
          </Body>
          <Right />
        </Header>
        {console.log('spinner:' + this.isFetching())}
        <Spinner
          visible= { this.isFetching() }
          textContent={''}
          textStyle={styles.spinnerTextStyle}
        />
        {this.renderComponent()}
      </Container>
    );
  }
}

function mapStateToProps(state){
  return {
    user : state.User,
    chargePointStatus : state.ChargePointStatus
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({userInfo, getUserData, updateUserData, initialiseChargePointStatus, getChargePointStatus, updateChargePointStatus}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(ChargingHomeScreen))

