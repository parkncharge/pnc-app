import React, {Component} from 'react';
import Ionicons from '@expo/vector-icons/Ionicons';
import MaterialIcons from '@expo/vector-icons/MaterialIcons';

import { createStackNavigator } from 'react-navigation';

// screens associated with the stack that runs off the home screen
import ChargingHomeScreen from './chargingHomeScreen';
import ChargingScreen2 from './chargingScreen2';


const chargingStackNavigator = createStackNavigator({
  ChargingHomeScreen:ChargingHomeScreen,
  ChargingScreen2:{screen: ChargingScreen2, navigationOptions: { tabBarVisible:false } },
},
{
  initialRouteName: 'ChargingHomeScreen',
  headerMode: "none"
});

chargingStackNavigator.navigationOptions = ({ navigation }) => {
  return {
    tabBarVisible: navigation.state.index === 0,
    title: 'Charging',
    tabBarLabel:"Charging",
    tabBarIcon: ({ tintColor }) => (
      <MaterialIcons name="power" color={tintColor} size={24}/>
    )
  };
};

export default chargingStackNavigator;