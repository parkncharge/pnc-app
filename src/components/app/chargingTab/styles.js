export default {
    container: {
      backgroundColor: "#FFF"
    },
    header: {
      backgroundColor:"#B0C4DE"
    },
    title:{
      fontSize:20,
      padding:10,
      textAlign: 'center'
    },
    label:{
      textAlign: 'left',
      fontSize:12,
      color:'#2F4F4F',
    },detailsView:{
      flex: 1
    }
  };