import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ActivityIndicator } from 'react-native';

class LoadingScreen extends Component {
    state = {
    }


    render () {
        return (
            <View style={styles.loadingScreen}>

            </View>
        
        ); 
    }
}



const styles = StyleSheet.create({
    loadingScreen: {
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0, 
        bottom: 0, 
        justifyContent: 'center', 
        alignItems: 'center',
        zIndex:10,
        opacity:0.9,
        backgroundColor:"#DCDCDC"
    },
    container: {
        width:150,
        height:150,
        backgroundColor:"black",
        justifyContent: 'center',
        alignItems:'center',
        borderRadius: 20,
        zIndex:20
    }
  });

export default LoadingScreen;