
import React, {Component} from 'react';
import { View } from "react-native";

import  { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import {rootReducer} from './store/reducers';
import {persistConfig} from './store/constants/persist-config';

import { persistStore, persistReducer } from 'redux-persist';

import { PersistGate } from 'redux-persist/lib/integration/react';

import { Root } from "native-base";

import { createSwitchNavigator } from 'react-navigation';

import AuthLoadingScreen from './components/authentication/authLoading';
import AuthStack from './components/authentication/navigator';
import AppStack from './components/app/navigator';

const createStoreWithMiddleware = applyMiddleware(promiseMiddleware())(createStore);



const pReducer = persistReducer(persistConfig, rootReducer);
const store = createStoreWithMiddleware(pReducer);
const persistor = persistStore(store);

const AppNavigator = createSwitchNavigator({
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
});


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Root>
            <AppNavigator/>
          </Root>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;


