
import { AsyncStorage } from 'react-native';
import { Toast } from 'native-base';
import axios from 'axios';

const BASE_NCR_URL = "http://chargepoints.dft.gov.uk/api/retrieve/registry/format/json/";
const BASE_PNC_URL = "https://us-central1-parkncharge-uk.cloudfunctions.net/";

const APP_KEY = 'uYou1buUQjOX0GVmnwDRVg';

import { FileSystem } from 'expo';

export const getNCRData = (region) => {
  let distance = Math.ceil(region.longitudeDelta * 100);
    if(distance > 30) distance = 30;
  let action = `lat/${region.latitude}/long/${region.longitude}/dist/${distance}/`;
  return invokeWebService(BASE_NCR_URL, action, 'get', 8000, null, null);
};

export const getPNCData = (region) => {
    let distance = Math.ceil(region.longitudeDelta * 100);
    if(distance > 30) distance = 30;

    let data = {
      "longitude":region.longitude,
	    "latitude":region.latitude,
	    "distance":distance
    };

    let action = "chargePoints";
    return invokeWebService(BASE_PNC_URL, action, 'post', 8000, data, null);
  };

export const invokeWebService = (url, action, method, timeout, data, progressCallback) => {
    console.log(url);
    console.log(action);
    console.log(data);
    /*
    const path = `${url}${action}`;
    console.log(path);

    FileSystem.downloadAsync(
      path,
      FileSystem.documentDirectory + 'sample.json'
    )
      .then(({ uri }) => {
        console.log('Finished downloading to ', uri);
      })
      .catch(error => {
        console.error(error);
      });

*/

    var promise = new Promise(function(resolve, reject){
/*
      let fileUri = FileSystem.documentDirectory + 'sample.json';
      FileSystem.readAsStringAsync(fileUri, null)
      .then((data) => {
        resolve(JSON.parse(data));
      })
      .catch((err) => {
        console.log(err);
      })
*/      
        axios({
            method: method,
            baseURL:url,
            url: action,
            data: data,
            headers: {
              'Authorization': 'token uYou1buUQjOX0GVmnwDRVg',
              'Content-Type': 'application/json'
            },
            timeout:timeout,
            onUploadProgress:progressCallback
          })
          .then(function(response){
            //console.log(response);
            if(response.headers['content-type'] !== "text/html" &&
               response.headers['Content-Type'] !== "text/html"){

              resolve(response.data);
            } else {
              console.log("content type: " + response.headers['Content-Type']);
              resolve([]);
            } 
          })
          .catch(function(err){
            console.log('Error in axios response');
            console.log(err);
            reject(err);
          })
          
    });

    return promise;
}

export const PNCWebService = (action, timeout, data) => {
  var promise = new Promise(function(resolve, reject){
    invokeWebService(BASE_PNC_URL, action, 'post', timeout, data, null)
    .then(function(response){
      resolve(response);
    })
    .catch(function(err){
      reject(err);
    });
  });
  return promise;
}

export const toastr = {
    showToast: (message, duration = 2500) => {
      Toast.show({
        text: message,
        buttonText: "OK",
        buttonTextStyle: { color: "#008000" },
        buttonStyle: { backgroundColor: "#5cb85c" },
        position: "bottom",
        duration:duration
      });
    }
  }
  

 


