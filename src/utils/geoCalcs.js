function bearingInitial (lat1, long1, lat2, long2)
{
    return (bearingDegrees(lat1, long1, lat2, long2) + 360) % 360;
}

function bearingFinal(lat1, long1, lat2, long2) {
    return (bearingDegrees(lat2, long2, lat1, long1) + 180) % 360;
}

function bearingDegrees (lat1, lon1, lat2, lon2){
		var degToRad = Math.PI/180.0;

    var lat1rad = lat1 * degToRad;
    var lat2rad = lat2 * degToRad;
    var lon1rad = lon1 * degToRad;
    var lon2rad = lon2 * degToRad;

		var dLon = (lon2rad - lon1rad);
	
    //we need to recalculate dLon if it is greater than pi
    if(Math.abs(dLon) > Math.PI) {
      if(dLon > 0) {
            dLon = ((2 * Math.PI) - dLon) * -1;
          }
          else {
            dLon = (2 * Math.PI) + dLon;
          }
    }
	
	
	//difference in the phi of latitudinal coordinates
   	dPhi = Math.log(Math.tan(lat2rad / 2 + Math.PI / 4) / Math.tan(lat1rad / 2 + Math.PI / 4));
   
   
   	var bearing = Math.round(((180 * (Math.atan2(dLon, dPhi)) / Math.PI)+ 360) % 360);
    return bearing;
}

function distanceKilometersGreatCircle (lat1, long1, lat2, long2)
{
    var degToRad= Math.PI / 180;
    var phi1= lat1 * degToRad;
    var phi2= lat2 * degToRad;
    var lam1= long1 * degToRad;
    var lam2= long2 * degToRad;

    return  6371.01 * Math.acos( Math.sin(phi1) * Math.sin(phi2) + Math.cos(phi1) * Math.cos(phi2) * Math.cos(lam2 - lam1) );
}

function distanceNMGreatCircle (lat1, long1, lat2, long2)
{
    var degToRad= Math.PI / 180;
    var phi1= lat1 * degToRad;
    var phi2= lat2 * degToRad;
    var lam1= long1 * degToRad;
    var lam2= long2 * degToRad;

    return  3440 * Math.acos( Math.sin(phi1) * Math.sin(phi2) + Math.cos(phi1) * Math.cos(phi2) * Math.cos(lam2 - lam1) );
}

function distanceByRoad(lat1, lon1, lat2, lon2){
    return 1.4 * distanceNMGreatCircle(lat1, lon1, lat2, lon2);
}

exports.bearingDegrees = bearingDegrees;
exports.distance = distanceNMGreatCircle;
exports.distanceByRoad = distanceByRoad;