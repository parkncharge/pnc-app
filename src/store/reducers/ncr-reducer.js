import { PENDING, FULFILLED, REJECTED, GET_NCR_LOCATIONS } from "../constants/action-types";


export default function(state={}, action){
    console.log(action.type);
    //console.log(action.payload);

    switch(action.type){
        case GET_NCR_LOCATIONS + PENDING:
            return {...state}

        case GET_NCR_LOCATIONS + FULFILLED:
            return {
                ...state,
                NCRLocations:action.payload,
                NCRLocationsError:false}

        case GET_NCR_LOCATIONS + REJECTED:
            return {
                ...state, 
                NCRLocationsErrorMessage:action.payload, 
                NCRLocationsError:true}

        
        default:
            return state;
    }
}

