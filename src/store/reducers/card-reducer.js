import { PENDING, FULFILLED, REJECTED, INITIALISE_CARDS, ADD_CARD, DELETE_CARD, GET_CARDS } from "../constants/action-types";

const _deleteCard = (array, index) => {
    console.log('array: ' + array);
    console.log('index: ' + index);
    let newArray = array.slice()
    newArray.splice(index, 1)
    return newArray
}

export default function(state={cardError:false, registeredCards:[]}, action){
    console.log(action.type);

    switch(action.type){
        case INITIALISE_CARDS:
            return {
                ...state,
                cardError:false,
                isFetching: false,
                registeredCards:action.payload
            }
        case ADD_CARD + PENDING:
            return {
                ...state,
                cardError:false,
                isFetching: true
            }

        case ADD_CARD + FULFILLED:
            return {
                ...state, 
                registeredCards:[...state.registeredCards, action.payload],
                cardError:false,
                isFetching: false
            };
            
        case ADD_CARD + REJECTED:
            return {
                ...state, 
                cardErrorMessage:action.payload, 
                cardError:true,
                isFetching: false}
        
        case DELETE_CARD + PENDING:
            return {
                ...state,
                cardError:false,
                isFetching: true
            }

        case DELETE_CARD + FULFILLED:
            return {
                ...state,
                registeredCards:_deleteCard(state.registeredCards, action.payload),
                cardError:false,
                isFetching: false
            }

        case DELETE_CARD + REJECTED:
            return {
                ...state, 
                cardErrorMessage:action.payload, 
                cardError:true,
                isFetching: false
            }
        

        case GET_CARDS + PENDING:
            return {
                ...state,
                cardError:false,
                isFetching: true
            }

        case GET_CARDS + REJECTED:
            return {
                ...state, 
                cardErrorMessage:action.payload, 
                cardError:true,
                isFetching: false
            }

        case GET_CARDS + FULFILLED:
            return {
                ...state,
                registeredCards:action.payload,
                cardError:false,
                isFetching: false
            }
        
        default:
            return state;
    }
}

