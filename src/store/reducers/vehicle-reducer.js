import { PENDING, FULFILLED, REJECTED, INITIALISE_VEHICLES, ADD_VEHICLE, DELETE_VEHICLE, UPDATE_VEHICLE, GET_VEHICLES } from "../constants/action-types";

const _deleteVehicle = (array, index) => {
    console.log('array: ' + array);
    console.log('index: ' + index);
    let newArray = array.slice()
    newArray.splice(index, 1)
    return newArray
}

export default function(state={vehicleError:false, registeredVehicles:[]}, action){
    console.log(action.type);

    switch(action.type){
        case INITIALISE_VEHICLES:
            return {
                ...state,
                vehicleError:false,
                isFetching: false,
                registeredVehicles:action.payload
            }
        case ADD_VEHICLE + PENDING:
            return {
                ...state,
                vehicleError:false,
                isFetching: true
            }

        case ADD_VEHICLE + FULFILLED:
            return {
                ...state, 
                registeredVehicles:[...state.registeredVehicles, action.payload],
                vehicleError:false,
                isFetching: false
            };
            
        case ADD_VEHICLE + REJECTED:
            return {
                ...state, 
                vehicleErrorMessage:action.payload, 
                vehicleError:true,
                isFetching: false}
        
        case DELETE_VEHICLE + PENDING:
            return {
                ...state,
                vehicleError:false,
                isFetching: true
            }

        case DELETE_VEHICLE + FULFILLED:
            return {
                ...state,
                registeredVehicles:_deleteVehicle(state.registeredVehicles, action.payload),
                vehicleError:false,
                isFetching: false
            }

        case DELETE_VEHICLE + REJECTED:
            return {
                ...state, 
                vehicleErrorMessage:action.payload, 
                vehicleError:true,
                isFetching: false
            }
        

        case GET_VEHICLES + PENDING:
            return {
                ...state,
                vehicleError:false,
                isFetching: true
            }

        case GET_VEHICLES + REJECTED:
            return {
                ...state, 
                vehicleErrorMessage:action.payload, 
                vehicleError:true,
                isFetching: false
            }

        case GET_VEHICLES + FULFILLED:
            return {
                ...state,
                registeredVehicles:action.payload,
                vehicleError:false,
                isFetching: false
            }
        
        default:
            return state;
    }
}

