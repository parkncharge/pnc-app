import { PENDING, FULFILLED, REJECTED, INITIALISE_CHARGE_POINT_STATUS, GET_CHARGE_POINT_STATUS, SET_CHARGE_POINT_STATUS} from "../constants/action-types";



export default function(state={chargingDataError:false, chargingData:null}, action){
    console.log(action.type);

    switch(action.type){
        case INITIALISE_CHARGE_POINT_STATUS:
            return {
                chargingData:null,
                chargingDataError:false,
                chargingDataErrorMessage:null,
                isFetching: false}

        case GET_CHARGE_POINT_STATUS + PENDING:
            return {
                ...state,
                isFetching: true
            }

        case GET_CHARGE_POINT_STATUS + FULFILLED:
            return {
                ...state,
                chargingData:action.payload,
                chargingDataError:false,
                chargingDataErrorMessage:null,
                isFetching: false}

        case GET_CHARGE_POINT_STATUS + REJECTED:
            return {
                ...state, 
                chargingDataErrorMessage:action.payload, 
                chargingDataError:true,
                isFetching: false}
        
        case SET_CHARGE_POINT_STATUS + PENDING:
            return {
                ...state,
                isFetching: true
            }

        case SET_CHARGE_POINT_STATUS + FULFILLED:
            return {
                ...state,
                chargingData:action.Payload,
                chargingDataError:false,
                isFetching: false}

        case SET_CHARGE_POINT_STATUS + REJECTED:
            return {
                ...state, 
                chargingDataErrorMessage:action.payload, 
                chargingDataError:true,
                isFetching: false}
        
        default:
            return state;
    }
}

