import { combineReducers } from 'redux';
import User from './user-reducer';
import Vehicles from './vehicle-reducer';
import NCRLocations from './ncr-reducer';
import ChargePointStatus from './chargePointStatus-reducer';
import Cards from './card-reducer';
import {persistConfig} from './../constants/persist-config';

import {LOGOUT, FULFILLED} from '../constants/action-types'

import { purgeStoredState } from 'redux-persist'


export const rootReducer = (state, action) => {
  if (action.type === LOGOUT + FULFILLED) {
    console.log('RESETTING STATE AND CLEARING PERSISTENT STORAGE');
    purgeStoredState(persistConfig);
    state = undefined;
  }
  return appReducer(state, action);
}

const appReducer = combineReducers({
  User,
  Vehicles,
  NCRLocations,
  ChargePointStatus,
  Cards
});







