import { 
    PENDING, 
    FULFILLED, 
    REJECTED, 
    REGISTER_USER, 
    LOGIN, 
    LOGOUT, 
    FORGOT_PASSWORD, 
    USER_INFO,
    GET_USER_DATA,
    SET_USER_DATA } from "../constants/action-types";

const initialState = {
    "loggedIn": false,
    "loginError": false,
    "logoutError": false,
    "userData": {
        "email": "",
        "emailVerified": false,
        "uid": "",
    },
    "profile":null,
    "appState":{
        "chargePointId":null,
    	"state":"idle"
    }
}


export default function(state=initialState, action){
    console.log(action.type);

    switch(action.type){
        case REGISTER_USER + PENDING:
            return {...state}

        case REGISTER_USER + FULFILLED:
            return {
                ...state,
                userData:{
                    email:action.payload.user.email,
                    emailVerified:action.payload.user.emailVerified,
                    uid:action.payload.user.uid,
                },
                registerError:false, 
                loggedIn:true}

        case REGISTER_USER + REJECTED:
            return {
                ...state, 
                userErrorMessage:action.payload, 
                registerError:true, 
                loggedIn:false}

        case LOGIN + PENDING:
            return {...state}

        case LOGIN + FULFILLED:
            return {
                ...state, 
                userData:{
                    email:action.payload.user.email,
                    emailVerified:action.payload.user.emailVerified,
                    uid:action.payload.user.uid,
                },
                loginError:false, 
                loggedIn:true}

        case LOGIN + REJECTED:
            return {
                ...state, 
                userErrorMessage:action.payload, 
                loginError:true, 
                loggedIn:false}

        case FORGOT_PASSWORD + PENDING:
            return {...state}

        case FORGOT_PASSWORD + FULFILLED:
            return {...state, resetPasswordError:false}

        case FORGOT_PASSWORD + REJECTED:
            return {
                ...state, 
                userErrorMessage:action.payload, 
                resetPasswordError:true
            }
            
        case LOGOUT + PENDING:
            return {...state}

        case LOGOUT + FULFILLED:
            return {...state, initialState}

        case LOGOUT + REJECTED:
            return {...state, initialState}

        case USER_INFO + PENDING:
            return {...state}

        case USER_INFO + FULFILLED:
            return {...state, user:action.payload}

        case USER_INFO + REJECTED:
            return {...state, userErrorMessage:action.payload}
         
        case SET_USER_DATA + PENDING:
            return {
                ...state,
                userDataError:false,
                isFetching: true
            }

        case SET_USER_DATA + FULFILLED:
            return {
                ...state, 
                profile:[...state.profile, action.payload.userData.profile],
                appState:[...state.appState, action.payload.userData.appState],
                userDataError:false,
                isFetching: false
            };
            
        case SET_USER_DATA + REJECTED:
            return {
                ...state, 
                userDataMessage:action.payload, 
                userDataError:true,
                isFetching: false
            }

            case GET_USER_DATA + PENDING:
            return {
                ...state,
                userDataError:false,
                isFetching: true
            }

        case GET_USER_DATA + REJECTED:
            return {
                ...state, 
                userDataMessage:action.payload, 
                userDataError:true,
                isFetching: false
            }

        case GET_USER_DATA + FULFILLED:
            return {
                ...state,
                profile:action.payload.userData.profile,
                appState:action.payload.userData.appState,
                userDataError:false,
                isFetching: false
            }
        default:
            return state;
    }
}

