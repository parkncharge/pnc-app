// src/js/constants/action-types.js

export const PENDING = "_PENDING";
export const FULFILLED = "_FULFILLED";
export const REJECTED = "_REJECTED";

//user
export const REGISTER_USER = "REGISTER_USER";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const FORGOT_PASSWORD = "FORGOT_PASSWORD";

//vehicles
export const INITIALISE_VEHICLES = "INITIALISE_VEHICLES";
export const ADD_VEHICLE = "ADD_VEHICLE";
export const DELETE_VEHICLE = "DELETE_VEHICLE";
export const GET_VEHICLES = "GET_VEHICLES";
export const UPDATE_VEHICLE = "UPDATE_VEHICLE";

//cards
export const INITIALISE_CARDS = "INITIALISE_CARDS";
export const ADD_CARD = "ADD_CARD";
export const DELETE_CARD = "DELETE_CARD";
export const GET_CARDS = "GET_CARDS";
export const UPDATE_CARD = "UPDATE_CARD";

//charging point locations
export const GET_NCR_LOCATIONS = "GET_NCR_LOCATIONS";

//charge point status
export const INITIALISE_CHARGE_POINT_STATUS = "INITIALISE_CHARGE_POINT_STATUS";
export const GET_CHARGE_POINT_STATUS = "GET_CHARGE_POINT_STATUS";
export const SET_CHARGE_POINT_STATUS = "SET_CHARGE_POINT_STATUS";

//user Data
export const GET_USER_DATA = "GET_USER_DATA";
export const SET_USER_DATA = "SET_USER_DATA";