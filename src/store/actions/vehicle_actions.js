
import { INITIALISE_VEHICLES, ADD_VEHICLE, DELETE_VEHICLE, GET_VEHICLES, UPDATE_VEHICLE } from "../constants/action-types";

import firebase from '../../firebase';
import { toastr, PNCWebService } from '../../utils/misc';

const _addVehicle = (data) => {
    var promise = new Promise(function(resolve, reject){
        PNCWebService("updateVehicle", 8000, data)
        .then(function(response){
            if(response.success){
                console.log('data:');
                console.log(data);
                resolve(data);
            } else {
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err);
        });
    });
    return promise;
}

const _deleteVehicle = (data, index) => {
    var promise = new Promise(function(resolve, reject){
        PNCWebService("deleteVehicle", 8000, data)
        .then(function(response){
            if(response.success){
                resolve(index);
            } else {
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }        
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err.message);
        });
    });
    return promise;
}

const _initialiseVehicles = () => {
    var promise = new Promise(function(resolve, reject){
        resolve([]);
    });

    return promise;
}

const _getVehicles = (user) => {
    var promise = new Promise(function(resolve, reject){
        const data = {userId:user.userData.uid};
        PNCWebService("listVehicles", 8000, data)
        .then(function(response){
            if(response.success){
                resolve(response.items);
            } else {
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }        
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err.message);
        });
    });
    return promise;
}

export function addVehicle(data) {
    return {
        type: ADD_VEHICLE,
        payload: _addVehicle(data)
    };
};

export function deleteVehicle(data, index) {
    return {
        type: DELETE_VEHICLE,
        payload: _deleteVehicle(data, index)
    };
};

export function initialiseVehicles() {
    console.log('initialising vehicles');
    return {
        type: INITIALISE_VEHICLES,
        payload: _initialiseVehicles()
    };
}

export function getVehicles(data) {
    return {
        type: GET_VEHICLES,
        payload: _getVehicles(data)
    };
};



