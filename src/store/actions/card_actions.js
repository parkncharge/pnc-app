
import { INITIALISE_CARDS, ADD_CARD, DELETE_CARD, GET_CARDS, UPDATE_CARD } from "../constants/action-types";

import firebase from '../../firebase';
import { toastr, PNCWebService } from '../../utils/misc';

const _addCard = (data) => {
    var promise = new Promise(function(resolve, reject){
        PNCWebService("updateCard", 8000, data)
        .then(function(response){
            if(response.success){
                console.log('data:');
                console.log(data);
                resolve(data);
            } else {
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err);
        });
    });
    return promise;
}

const _deleteCard = (data, index) => {
    var promise = new Promise(function(resolve, reject){
        PNCWebService("deleteCard", 8000, data)
        .then(function(response){
            if(response.success){
                resolve(index);
            } else {
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }        
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err.message);
        });
    });
    return promise;
}

const _initialiseCards = () => {
    var promise = new Promise(function(resolve, reject){
        resolve([]);
    });

    return promise;
}

const _getCards = (user) => {
    var promise = new Promise(function(resolve, reject){
        const data = {userId:user.userData.uid};
        PNCWebService("listCards", 8000, data)
        .then(function(response){
            if(response.success){
                resolve(response.items);
            } else {
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }        
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err.message);
        });
    });
    return promise;
}

export function addCard(data) {
    return {
        type: ADD_CARD,
        payload: _addCard(data)
    };
};

export function deleteCard(data, index) {
    return {
        type: DELETE_CARD,
        payload: _deleteCard(data, index)
    };
};

export function initialiseCards() {
    console.log('initialising cards');
    return {
        type: INITIALISE_CARDS,
        payload: _initialiseCards()
    };
}

export function getCards(data) {
    return {
        type: GET_CARDS,
        payload: _getCards(data)
    };
};

export function updateCard(data) {
    return {
        type: UPDATE_CARD,
        payload: _updateCard(data)
    };
};

