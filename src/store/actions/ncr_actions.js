
import { GET_NCR_LOCATIONS } from "../constants/action-types";

import { toastr, getNCRData } from '../../utils/misc';

const _getNCRLocations = (data) => {
    var promise = new Promise(function(resolve, reject){
        let action = `lat/${data.latitude}/long/${data.longitude}/dist/${data.distance}/`;
        getNCRData(action)
        .then(function(response){
            console.log('==========');
            console.log(response.ChargeDevice);
            console.log('==========');
            resolve(response.ChargeDevice);
        })
        .catch(function(err){
             console.log(err);
             toastr.showToast(err.message);
             reject(err.message);
        })
    });

    return promise;
}

export function getNCRLocations(data) {
    return {
        type: GET_NCR_LOCATIONS,
        payload: _getNCRLocations(data)
    };
};