

import { 
    REGISTER_USER, 
    LOGIN, 
    FORGOT_PASSWORD, 
    USER_INFO, 
    LOGOUT,
    GET_USER_DATA,
    SET_USER_DATA
 } from "../constants/action-types";

import firebase from '../../firebase';
import { toastr, PNCWebService } from '../../utils/misc';

const signUpUser = (data) => {
    var promise = new Promise(function(resolve, reject){

        firebase.auth().createUserWithEmailAndPassword(data.email, data.password)
        .then(function(response){
            resolve(response);
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err.message);
        })
    });

    return promise;
}

const loginUser = (data) => {
    var promise = new Promise(function(resolve, reject){

        firebase.auth().signInWithEmailAndPassword(data.email, data.password)
        .then(function(response){
            resolve(response);
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err.message);
        })
    });
    return promise;
}

const sendPasswordReset = (emailAddress) => {
    var promise = new Promise(function(resolve, reject){
       const ERROR_MESSAGE = "If we have an account with this password we will send a reset link to it";

        firebase.auth().sendPasswordResetEmail(emailAddress)
        .then(function() {
            toastr.showToast(ERROR_MESSAGE);
            console.log();
            resolve();
        })
        .catch(function(err) {
            if(err.code === 'auth/user-not-found'){
                toastr.showToast(ERROR_MESSAGE);
                resolve();
            } else {
                toastr.showToast(err.message);
                reject(err.message);
            }
        });
    });
    return promise;
}

const signOut = () => {
    var promise = new Promise(function(resolve, reject){
        firebase.auth().signOut().then(function(response) {
            resolve(response);
          }, function(err) {
            toastr.showToast(err.message);
            reject(err.message);
          });
    });
    return promise;
}

const currentUser = () => {
    var promise = new Promise(function(resolve, reject){
        console.log('getting current user');
        this.authSubscription = firebase.auth().onAuthStateChanged((user) => {
            console.log(user);
            this.authSubscription();
            resolve(user);
          });
    });
    return promise;
}

const _updateUserData = (data) => {
    var promise = new Promise(function(resolve, reject){
        PNCWebService("updateUserData", 8000, data)
        .then(function(response){
            if(response.success){
                console.log('data:');
                console.log(data);
                resolve(data);
            } else {
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err);
        });
    });
    return promise;
}

const _getUserData = (user) => {
    var promise = new Promise(function(resolve, reject){
        const data = {userId:user.userData.uid};
        PNCWebService("getUserData", 8000, data)
        .then(function(response){
            if(response.success){
                resolve(response);
            } else {
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }        
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err.message);
        });
    });
    return promise;
}

export function signUp(data) {
    return {
        type: REGISTER_USER,
        payload: signUpUser(data)
    };
};


export const login = (data) => ({
    type: LOGIN,
    payload: loginUser(data)
});

export const forgotPassword = (email) => ({
    type: FORGOT_PASSWORD,
    payload: sendPasswordReset(email)
});

export const logOut = (email) => ({
    type: LOGOUT,
    payload: signOut()
});

export const userInfo = () => ({
    type: USER_INFO,
    payload: currentUser()
});

export function getUserData(data) {
    return {
        type: GET_USER_DATA,
        payload: _getUserData(data)
    };
};

export function updateUserData(data) {
    return {
        type: SET_USER_DATA,
        payload: _updateUserData(data)
    };
};