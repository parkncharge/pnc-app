
import { INITIALISE_CHARGE_POINT_STATUS, GET_CHARGE_POINT_STATUS, SET_CHARGE_POINT_STATUS} from "../constants/action-types";


import firebase from '../../firebase';
import { toastr, PNCWebService } from '../../utils/misc';

const _updateChargePointStatus = (data) => {
    var promise = new Promise(function(resolve, reject){
        PNCWebService("updatePNCChargePointStatus", 8000, data)
        .then(function(response){
            if(response.success){
                console.log('data:');
                console.log(data);
                resolve(data.value);
            } else {
                console.log(response);
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err);
        });
    });
    return promise;
}



const _getChargePointStatus = (deviceId, connectorId) => {
    var promise = new Promise(function(resolve, reject){
        const data = {
            DeviceId:deviceId,
            ConnectorId:connectorId
        };
        PNCWebService("getPNCChargePointStatus", 8000, data)
        .then(function(response){
            if(response.success){
                resolve(response);
            } else {
                console.log(response);
                const err = new Error("unable to access database, are you connected to the internet?");
                toastr.showToast(err.message);
                reject(err.message);
            }        
        })
        .catch(function(err){
            toastr.showToast(err.message);
            reject(err.message);
        });
    });
    return promise;
}

export function initialiseChargePointStatus() {
    return {
        type: INITIALISE_CHARGE_POINT_STATUS,
        payload: null
    };
}

export function updateChargePointStatus(data) {
    return {
        type: SET_CHARGE_POINT_STATUS,
        payload: _updateChargePointStatus(data)
    };
};


export function getChargePointStatus(deviceId, connectorId) {
    console.log(deviceId);
    console.log(connectorId);
    return {
        type: GET_CHARGE_POINT_STATUS,
        payload: _getChargePointStatus(deviceId, connectorId)
    };
};



